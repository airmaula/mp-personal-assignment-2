import {Text} from "react-native-paper";
import {StyleSheet, View} from "react-native";
import React from "react";

export default function Homepage() {
    return (
        <View style={styles.container}>
            <Text variant="headlineMedium">Selamat datang!</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});