import {Text} from "react-native-paper";
import {StyleSheet, View} from "react-native";
import React from "react";
import MapView from 'react-native-maps';

export default function Maps() {
    return (
        <View>
            <MapView
                style={{
                    width: '100%',
                    height: 500,
                    backgroundColor: 'red'
                }}
                initialRegion={{
                    latitude: 37.78825,
                    longitude: -122.4324,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                }}
                onRegionChangeComplete={(e) => {
                    console.log(e)
                }}
            />
        </View>
    )
}