import {Button, Text, TextInput} from "react-native-paper";
import {StyleSheet, View} from "react-native";
import React, {useState} from "react";
import {useProfile} from "../context/profile";

export default function Form() {

    const {setName} = useProfile()

    const [form, setForm] = useState({
        nama: '',
        hobi: '',
    })

    const saveData = () => {
        setName(form.nama)
        alert('Data tersimpan')
    }

    return (
        <View style={styles.container}>
            <Text variant="titleLarge" style={styles.mb}>Form Data Diri</Text>
            <TextInput
                mode={'outlined'}
                label={'Nama'}
                style={styles.input}
                value={form.nama}
                onChangeText={(value) => {
                    setForm(prevState => ({
                        ...prevState,
                        nama: value
                    }))
                }}
            />
            <TextInput
                mode={'outlined'}
                label={'Hobi'}
                style={styles.input}
                onChangeText={(value) => {
                    setForm(prevState => ({
                        ...prevState,
                        hobi: value
                    }))
                }}
            />
            <Button icon="content-save" mode="contained" onPress={saveData} style={styles.button}>
                Simpan
            </Button>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 16
    },
    mb: {
        marginBottom: 8,
    },
    input: {
        marginBottom: 8,
    },
    button: {
        borderRadius: 6,
        marginTop: 8
    }
});