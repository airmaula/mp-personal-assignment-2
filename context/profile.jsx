import {createContext, useContext, useState} from "react";

const ProfileContext = createContext({
    name: 'Guest'
})

export const ProfileProvider = ({children}) => {
    const [name, setName] = useState('Guest')
    return (
        <ProfileContext.Provider value={{
            name,
            setName
        }}>
            {children}
        </ProfileContext.Provider>
    )
}

export const useProfile = () => useContext(ProfileContext)