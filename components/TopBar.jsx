import {Appbar} from "react-native-paper";
import {useProfile} from "../context/profile";

export default function TopBar() {
    const {name} = useProfile()
    return (
        <Appbar.Header>
            <Appbar.Content title={name} />
        </Appbar.Header>
    )
}

