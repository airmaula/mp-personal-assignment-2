import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { MD3LightTheme as DefaultTheme, PaperProvider } from 'react-native-paper';
import TopBar from "./components/TopBar";
import BottomNav from "./components/BottomNav";
import {NavigationContainer} from "@react-navigation/native";
import {ProfileProvider} from "./context/profile";

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
  },
};

export default function App() {
  return (
      <ProfileProvider>
          <PaperProvider theme={theme}>
            <TopBar/>

            <NavigationContainer>
              <BottomNav/>
            </NavigationContainer>
          </PaperProvider>
      </ProfileProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
